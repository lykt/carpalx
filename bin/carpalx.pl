
=pod

=head1 NAME

carpalx - given text input, determine optimal keyboard mapping to minimize typing effort based on a typing effort model

=head1 SYNOPSIS

  # all configuration read from etc/carpalx.conf
  carpalx -keyboard_input keyboard.conf
          -corpus corpus/words.txt
          -action loadkeyboard,loadtriads,optimize
          -conf etc/carpalx.conf
          [-debug]

=head1 DESCRIPTION

carpalx is a keyboard layout optimizer. Given a training corpus
(e.g. English text) and parameters that describe typing effort,
carpalx uses simulated annealing to find a keyboard layout to minimize
typing effort.

Typing effort is modeled using three contributions. First, base effort
is derived from finger travel distance. Second, row, hand and finger
penalties are added to limit use of weaker fingers/hands and
distinguish harder-to-reach keys. Third, stroke path effort is used to
rate the effort based on finger, row and hand alternation (e.g. asd is
much easier to type than sad).

=head1 CONFIGURATION

=head2 Configuration file name and path

carpalx will look in the following locations for a configuration file

  .
  SCRIPT_BIN/../etc
  SCRIPT_BIN/etc
  SCRIPT_BIN/
  ~/.carpalx/etc
  ~/.carpalx

where SCRIPT_BIN is the location of the carpalx script. If the name of
the configuration file is not passed via -conf, then SCRIPT_NAME.conf
is tried where SCRIPT_NAME is the name of the script. For example,

  > cd carpalx-0.11
  > bin/carpalx

will attempt to find carpalx.conf in the above paths.

Using -debug -debug will dump the configuration parameters.

  > bin/carpalx -debug -debug

=head2 Configuration structure

The configuration file comprises variable-value pairs, which may be
placed in blocks.

  a = 1
  <someblock>
    b = 2
    <anotherblock>
    c = 3
    </anotherblock>
  </someblock>

Combinations of related parameters (e.g. base effort, keyboard
configuration) are stored in individual files
(e.g. etc/mask/letters.conf) which are subsequently imported into
the main configuration file using <<include>>

  ...
  <<include etc/mask/letters.conf>>
  ...

=head1 HISTORY

=over

=item * 0.10

Packaged and versioned code.

=item * 0.11

Adjusted typing model to include weights for base, effort and stroke components.

Improved clarity of effort reports.

Improved consistency in configuration file.

Added fonts/

=item * 0.12

Can now load a cache file basedon parsed corpus instead of original corpus.

=back

=head1 BUGS

Report!

=head1 AUTHOR

Martin Krzywinski <martink@bcgsc.ca>
http://mkweb.bcgsc.ca

=head1 CONTACT

  Martin Krzywinski
  Genome Sciences Centre
  100-570 W 7th Ave 
  Vancouver BC V5Z 4S6

=cut

################################################################
#
# Copyright 2002-2014 Martin Krzywinski <martink@bcgsc.ca> http://mkweb.bcgsc.ca
#
# This file is part of the Genome Sciences Centre Perl code base.
#
# This script is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
################################################################

################################################################
#                             ___   __
#                            | \ \ / /
#   ___ __ _ _ __ _ __   __ _| |\ V /
#  / __/ _` | '__| '_ \ / _` | | > <
# | (_| (_| | |  | |_) | (_| | |/ . \
#  \___\__,_|_|  | .__/ \__,_|_/_/ \_\
#                | |
#                |_| v0.11
#
# carpalX - keyboard layout optimizer - save your carpals
#
# Face it, typing for 10 years can leave your hands looking like
# cranky twigs. Moving that pinky over and over again and rotating
# the wrist - ouch.
#
# CarpalX processes a document and computes the total carpal
# effort required to type it using a default qwerty keyboard
# layout. Effort of each key is defined by its location on the
# keyboard as well as the finger customarily responsible for hitting
# that key.
#
# Long-range effects (double and triple) key combinations
# contribute to 1st and 2nd order effort quantities. For example,
# it requires more effort to type aaazzzaaa than zaazaazaa, because
# the wrist rotation is prolonged and the pinky must extend to hit
# three z's in a row.
#
# You have the option to optimize the keyboard layout for the
# document. The simulated annealing method is used to determine
# a key layout that minimizes total effort.
#
################################################################

use strict;
use utf8;
use JSON::PP;
use Cwd;
use Config::General;
use Data::Dumper;
use File::Basename;
use File::Spec;
use FindBin;
use Getopt::Long;
use GD;
use IO::File;
use Math::VecStat qw(sum min max average);
use Set::IntSpan;
use Pod::Usage;
use Storable qw(store retrieve dclone);
use Digest::MD5 qw(md5_hex);
use Time::HiRes qw(gettimeofday tv_interval);
use lib "$FindBin::RealBin";
use lib "$FindBin::RealBin/../lib";
use lib "$FindBin::RealBin/lib";
use vars qw(%OPT %CONF);

GetOptions(\%OPT,
					 "keyboard_input=s",
					 "keyboard_output=s",
					 "action=s",
					 "corpus=s",
					 "words=s",
					 "wordlength=s",
					 "detail",
					 "mode=s",
					 "triads_max_num=i",
					 "triads_overlap",
					 "cdump",
					 "configfile=s","help","man","debug+");

binmode STDOUT, ":utf8";

pod2usage() if $OPT{help};
pod2usage(-verbose=>2) if $OPT{man};
loadconfiguration($OPT{configfile});
populateconfiguration(); # copy command line options to config hash
if($CONF{cdump}) {
  $Data::Dumper::Pad = "debug parameters";
  $Data::Dumper::Indent = 1;
  $Data::Dumper::Quotekeys = 0;
  $Data::Dumper::Terse = 1;
  print Dumper(\%CONF);
	exit;
}

my @actions = split(/,/,$CONF{action});

my ($keytriads,$keyboard);

while(my $action = shift(@actions)) {
  printdebug(1,"found action",$action);
  if($action =~ /loadtriads?/i) {
    ################################################################
    #
    # read the document and extract triads
    #
    # triads are adjacent three-key combinations parsed from the
    # document based on the setting of the mode=MODE value
    # (see <mode MODE> block for filters for the MODE).
    #
    printdebug(1,"loading triad from corpus file",$CONF{corpus});
    $keytriads = read_document($CONF{corpus});
  } elsif ($action =~ /loadkeyboard/i) {
    ################################################################
    #
    # create a keyboard and the associated effort matrix
    #
    printdebug(1,"loading keyboard from",$CONF{keyboard_input});
    $keyboard = create_keyboard($CONF{keyboard_input});
  } elsif ($action =~ /reportt?riads?/i) {
    printdebug(1,"reporting triad frequency");
    report_triads($keytriads,$keyboard);
  } elsif ($action =~ /reportword(effort|data)/i) {
    printdebug(1,"reporting word efforts");
    report_word_effort($keyboard,$1);
  } elsif ($action =~ /reporteffort(.*)/i) {
    # calculate the canonical effort associated with the original
    # keyboard layout - the layout will be altered to try to minimize this
    my $effort_canonical;
    printdebug(1,"calculating effort");
    $CONF{memorize} = 0;
    report_keyboard_effort($keytriads,$keyboard,$1);
    $CONF{memorize} = 1;
  } elsif ($action =~ "optimize") {
    # optimize the keyboard layout to decrease the effort
    my $timer = [gettimeofday];
    $keyboard = optimize_keyboard($keytriads,$keyboard);
    $timer = tv_interval($timer);
    printkeyboard($keyboard);
    print "Total time spent optimizing: $timer s\n";
  } elsif ($action =~ /(exit|quit)/) {
    exit;
  } else {
    die "cannot understand action $action";
  }
}

exit;

################################################################
################################################################

=pod

=head1 INTERNAL FUNCTIONS

The content below may be out of date

=cut

sub report_keyboard_effort {

  my ($keytriads,$keyboard,$option) = @_;

  my %effort;
  $effort{all}       = calculate_effort($keytriads,$keyboard);

  my %CONF_prev      = dclone(\%CONF);

  # recall triad effort is 
  #
  # $kb * $k1*$be1 * ( 1 + $k2*$be2 * ( 1 + $k3*$be3 ) ) + 
  # $kp * $k1*$pe1 * ( 1 + $k2*$pe2 * ( 1 + $k3*$pe3 ) ) + 
  # $ks * $s
  #
  # be1,be2,be3 baseline efforts for first, second and third key in triad   
  # be1,be2,be3 penalty efforts for first, second and third key in triad   
  # s stroke path

  {
    # baseline effort
    # kp=ks=0
    local $CONF{effort_model}{k_param}{kp}                             = 0;
    local $CONF{effort_model}{k_param}{ks}                             = 0;
    my $keyboard_new   = create_keyboard($CONF{keyboard_input});
    $effort{base}    = calculate_effort($keytriads,$keyboard_new);
  }
  {
    # penalty effort
    # kb=ks=0
    local $CONF{effort_model}{k_param}{kb}                             = 0;
    local $CONF{effort_model}{k_param}{ks}                             = 0;
    my $keyboard_new = create_keyboard($CONF{keyboard_input});
    $effort{penalty}= calculate_effort($keytriads,$keyboard_new);
  }
  {
    # stroke effort
    # kb=kp=0
    local $CONF{effort_model}{k_param}{kb}                             = 0;
    local $CONF{effort_model}{k_param}{kp}                             = 0;
    my $keyboard_new = create_keyboard($CONF{keyboard_input});
    $effort{path}= calculate_effort($keytriads,$keyboard_new);
  }
  {
    # hand penalty only
    local $CONF{effort_model}{k_param}{kb}                             = 0;
    local $CONF{effort_model}{k_param}{ks}                             = 0;
    local $CONF{effort_model}{weight_param}{penalties}{default}        = 0;
    local $CONF{effort_model}{weight_param}{penalties}{weight}{row}    = 0;
    local $CONF{effort_model}{weight_param}{penalties}{weight}{finger} = 0;
    my $keyboard_new = create_keyboard($CONF{keyboard_input});
    $effort{penalty_hand}= calculate_effort($keytriads,$keyboard_new);
  }
  {
    # row penalty only
    local $CONF{effort_model}{k_param}{kb}                             = 0;
    local $CONF{effort_model}{k_param}{ks}                             = 0;
    local $CONF{effort_model}{weight_param}{penalties}{default}        = 0;
    local $CONF{effort_model}{weight_param}{penalties}{weight}{hand}   = 0;
    local $CONF{effort_model}{weight_param}{penalties}{weight}{finger} = 0;
    my $keyboard_new = create_keyboard($CONF{keyboard_input});
    $effort{penalty_row}= calculate_effort($keytriads,$keyboard_new);
  }
  {
    # finger penalty only
    local $CONF{effort_model}{k_param}{kb}                             = 0;
    local $CONF{effort_model}{k_param}{ks}                             = 0;
    local $CONF{effort_model}{weight_param}{penalties}{default}        = 0;
    local $CONF{effort_model}{weight_param}{penalties}{weight}{hand}   = 0;
    local $CONF{effort_model}{weight_param}{penalties}{weight}{row}    = 0;
    my $keyboard_new = create_keyboard($CONF{keyboard_input});
    $effort{penalty_finger}= calculate_effort($keytriads,$keyboard_new);
  }

  {
    # one-key effort
    local $CONF{effort_model}{k_param}{k2}                             = 0;
    local $CONF{effort_model}{k_param}{k3}                             = 0;
    local $CONF{effort_model}{k_param}{ks}                             = 0;
    my $keyboard_new = create_keyboard($CONF{keyboard_input});
    $effort{k1}= calculate_effort($keytriads,$keyboard_new);
  }
  {
    # two-key effort 
    local $CONF{effort_model}{k_param}{k3}                             = 0;
    local $CONF{effort_model}{k_param}{ks}                             = 0;
    my $keyboard_new = create_keyboard($CONF{keyboard_input});
    $effort{k12}= calculate_effort($keytriads,$keyboard_new);
  }
  {
    # three-key effort 
    local $CONF{effort_model}{k_param}{ks}                             = 0;
    my $keyboard_new = create_keyboard($CONF{keyboard_input});
    $effort{k123}= calculate_effort($keytriads,$keyboard_new);
  }

  printinfo("Keyboard effort");
  printinfo("-"x60);

  my %efforts = ( k1 => [ $effort{k1},
			  100*sdiv($effort{k1},$effort{k123}),
			  100*sdiv($effort{k1},$effort{k123})],
		  k12 => [ $effort{k12},
			   100*sdiv($effort{k12}-$effort{k1},$effort{k123}),
			   100*sdiv($effort{k12},$effort{k123}) ],
		  k123 => [ $effort{k123},
			    100*sdiv($effort{k123}-$effort{k12},$effort{k123}),
			    100*sdiv($effort{k123},$effort{k123})],
		  base => [ $effort{base},
			    100*sdiv($effort{base},$effort{all}),
			    100*sdiv($effort{base},$effort{all}) ],
		  penalty => [ $effort{penalty},
			       100*sdiv($effort{penalty},$effort{all}),
			       100*sdiv($effort{base}+$effort{penalty},$effort{all}) ],
		  penalty_hand => [ $effort{penalty_hand},
			       100*sdiv($effort{penalty_hand},$effort{penalty}),
			       100*sdiv($effort{penalty_hand},$effort{penalty}) ],
		  penalty_row => [ $effort{penalty_row},
			       100*sdiv($effort{penalty_row},$effort{penalty}),
			       100*sdiv($effort{penalty_hand}+$effort{penalty_row},$effort{penalty}) ],
		  penalty_finger => [ $effort{penalty_finger},
			       100*sdiv($effort{penalty_finger},$effort{penalty}),
			       100*sdiv($effort{penalty_hand}+$effort{penalty_row}+$effort{penalty_finger},$effort{penalty}) ],
		  path => [ $effort{path},
			    100*sdiv($effort{path},$effort{all}),
			    100*sdiv($effort{base}+$effort{penalty}+$effort{path},$effort{all}) ],
		  all => [ $effort{all},
			    100*sdiv($effort{all},$effort{all}),
			    100*sdiv($effort{all},$effort{all}) ],
		);

  printinfo(sprintf("%-20s %8.3f %5.1f %5.1f","k1",
		    @{$efforts{k1}}));
  printinfo(sprintf("%-20s %8.3f %5.1f %5.1f","k1,k2",
		    @{$efforts{k12}}));
  printinfo(sprintf("%-20s %8.3f %5.1f %5.1f","k1,k2,k3",
		    @{$efforts{k123}}));
  printinfo(sprintf("%-20s %8.3f %5.1f %5.1f","b",
		    @{$efforts{base}}));
  printinfo(sprintf("%-20s %8.3f %5.1f %5.1f","p",
		    @{$efforts{penalty}}));
  printinfo(sprintf("%-20s %8.3f %5.1f %5.1f","ph",
		    @{$efforts{penalty_hand}}));
  printinfo(sprintf("%-20s %8.3f %5.1f %5.1f","pr",
		    @{$efforts{penalty_row}}));
  printinfo(sprintf("%-20s %8.3f %5.1f %5.1f","pf",
		    @{$efforts{penalty_finger}}));
  printinfo(sprintf("%-20s %8.3f %5.1f %5.1f","s",
		    @{$efforts{path}}));
  printinfo(sprintf("%-20s %8.3f %5.1f %5.1f","all",
		    @{$efforts{all}}));

  return if $option eq "verybrief";
}

sub sdiv {
  my ($x,$y) = @_;
  return $y ? $x/$y : 0;
}

# report the frequency and cumulative frequency of all triads

sub find_action {
  my ($rx,@actions) = @_;     
  if ( my ($action) = grep($rx,@actions) ) {
    return $action;
  } else {
    return undef;
  }
}

sub advance_actions {
  my ($action,@actions) = @_;
  exit if $action =~ /exit|quit/;
  my @newactions;
  my $found;
  for $a (@actions) {
    if(! $found && $a eq $action) {
      $found = 1;
      next;
    }
    push @newactions, $a;
  }
  return @newactions;
}

sub report_triads {
  my ($keytriads,$keyboard) = @_;
  my $n = sum(values %$keytriads);
  my $nc = 0;
  for my $triad (sort {$keytriads->{$b} <=> $keytriads->{$a}} keys %$keytriads) {
    $nc += $keytriads->{$triad};
    my $effort = $keyboard ? calculate_triad_effort($triad,$keyboard,@{$CONF{effort_model}{k_param}}{qw(k1 k2 k3 kb kp ks)}) : "na";
    printinfo("triad",$triad,$keytriads->{$triad},$keytriads->{$triad}/$n,$nc/$n,"effort",$effort);
  }
}

sub resolve_path {
  my $file = shift;
	my $is_absolute = File::Spec->file_name_is_absolute( $file );
  if($is_absolute) {
    return $file;
  } else {
    return "$CONF{configdir}/$file";
  }
}

sub report_word_effort {
    my ($keyboard,$option) = @_;
    open(WORDS, "<:utf8", $CONF{words} =~ /^\// ? $CONF{words} : $FindBin::RealBin . "/$CONF{words}") || die "cannot open word list file $CONF{words}";
    my @words = <WORDS>;
    chomp @words;
    close(WORDS);
    if($CONF{wordlength}) {
      my $length = Set::IntSpan->new($CONF{wordlength});
      @words = grep($length->member(length($_)), @words);
    }
    #@words = grep($_ =~ /^[a-z]+$/, map { lc $_ } @words);
    my $wordeffort = rankwords(\@words,$keyboard);
    summarizerankwords($wordeffort,$option);
}

################################################################
# 
# given a list of words, return the words and associated
# efforts, as calculated using the triads of the words
#
################################################################

sub rankwords {
  my $words = shift;
  my $keyboard = shift;
  my $wordeffort;
  foreach my $word (@$words) {
    my $wordtriads;
    while($word =~ /(...)/g) {
      my $triad = lc $1;
      $wordtriads->{$triad}++;
      pos $word -= 2;
    }
    next unless keys %$wordtriads;
    my $word_effort = calculate_effort($wordtriads,$keyboard);
    $wordeffort->{lc $word} = $word_effort;
    printdebug(1,"wordeffort",lc $word,$word_effort);
  }
  return $wordeffort;
}

################################################################
#
# Produce a summary of the word effort statistics
#
################################################################

sub summarizerankwords {
  my ($wordeffort,$option) = @_;
  if($option eq "data") {
    print(JSON::PP->new->pretty()->encode($wordeffort));
  }
  else {
    my @sorted = (sort {$wordeffort->{$b} <=> $wordeffort->{$a}} keys %$wordeffort);
    for my $word (@sorted) {
      printinfo($word,$wordeffort->{$word});
    }
  }
}

=pod

=head2 optimize_keyboard()

  $newkeyboard = optimize_keyboard($keytriads,$keyboard);

Simulated annealing is used to search for a better keyboard layout. The function uses the list of triads, generated from the input text document, and an initial keyboard layout.

=cut

sub optimize_keyboard {
  die "more arguments needed in optimize_keyboard" unless @_ == 2;
  my ($keytriads,$keyboard) = @_;
  my $effort       = calculate_effort($keytriads,$keyboard);
  my $iterations   = $CONF{annealing}{iterations} || 1000;
  my ($t0,$k)      = @{$CONF{annealing}}{qw(t0 k)};
  # load up the mask - eligible keys for relocation
  my $mask         = _parse_mask($CONF{maskfilename});
  die "cannot create mask" unless $mask;
  # create a list of all keys that can be relocated
  my $reloc_list   = make_relocatable_list($mask);
  my $update_count = 0;
  my $last_reported_effort;

  my $keyboard_original = dclone($keyboard);
  my %seen_digests;
  for my $iter (1..$iterations) {
    my $time = [gettimeofday];
    my $keyboardnew;

    my $swap_range = $CONF{annealing}{maxswaps} - $CONF{annealing}{minswaps};
    my $swap_num   = $CONF{annealing}{minswaps};

    $swap_num      += int(rand($swap_range+1)) if $swap_range;
    if($CONF{annealing}{onestep}) {
      $keyboardnew     = _swap_keys($keyboard_original,$reloc_list,$swap_num);
      $effort          = calculate_effort($keytriads,$keyboard_original);
      my $digest = keyboard_digest($keyboardnew);
      if($seen_digests{$digest}) {
	# already seen this layout - fetch next layout
	next;
      }
      $seen_digests{$digest}++;
    } else {
      $keyboardnew     = _swap_keys($keyboard,$reloc_list,$swap_num);
    }
    # this is breaking???
    my $effortnew       = calculate_effort($keytriads,$keyboardnew);
    my $deffort         = $effortnew - $effort;
    my %report;
    $report{effort}    = $effort;
    $report{neweffort} = $effortnew;
    $report{deffort}   = $deffort;
    my $t = $t0*exp(-$iter*$k/$iterations);
    my $p = $CONF{annealing}{p0} * exp(-abs($deffort)/$t);
    $p = 1 if $p > 1; # float round-off
    $report{t} = $t;
    $report{p} = $p;

    my $keyboard_is_updated = 0;

    if( ($CONF{annealing}{action} eq "minimize" && $deffort < 0) ||
	($CONF{annealing}{action} eq "maximize" && $deffort > 0) ) {
      # always accept layouts for which the effort is lower/higher (as prescribed by action)
      $effort   = $effortnew;
      $keyboard = $keyboardnew;
      $report{move} = "better/accept";
      $keyboard_is_updated = 1;
    } else {
      # sometimes accept layouts for which the effort is higher/lower (as prescribed by action)
      if(rand() < $p) {
	$report{move} = "worse/accept";
	$effort = $effortnew;
	$keyboard = $keyboardnew;
	$keyboard_is_updated = 1;
      } else {
	$report{move} = "worse/reject";
      }
    }
    $update_count += $keyboard_is_updated;

    my $stdout_report;
    if($CONF{stdout_filter} eq "all") {
      $stdout_report = 1;
    } elsif ($CONF{stdout_filter} eq "update") {
      $stdout_report = 1 if $report{move} =~ /accept/;
    } elsif ($CONF{stdout_filter} eq "lower") {
      $stdout_report = 1 if $deffort < 0;
    } elsif ($CONF{stdout_filter} eq "higher") {
      $stdout_report = 1 if $deffort > 0;
    } elsif ($CONF{stdout_filter} eq "lower_monotonic") {
      $stdout_report = 1 if ! defined $last_reported_effort || $effortnew < $last_reported_effort;
    } elsif ($CONF{stdout_filter} eq "higher_monotonic") {
      $stdout_report = 1 if ! defined $last_reported_effort || $effortnew > $last_reported_effort;
    }

    my $parameters = {t=>$t,iter=>$iter,update_count=>$update_count,effort=>$effortnew,deffort=>$deffort};

    my $elapsed = tv_interval($time);

    # output to STDOUT is always lower monotonic
    if($stdout_report) {
      printkeyboard($keyboardnew);
      printf ("iter %6d effort %8.6f -> %8.6f d %10.8f p %10.8f t %10.8f %s cpu %s\n",
	      $iter,@report{qw(effort neweffort deffort p t move)},$elapsed);
    }

  }
  return $keyboard;
}

sub keyboard_digest {
  my $keyboard = shift;
  my @keys;
  for my $row (0..@{$keyboard->{key}}-1) {
    for my $col (0..@{$keyboard->{key}[$row]}-1) {
      push @keys, join("",@{$keyboard->{key}[$row][$col]}{qw(lc)});
    }
  }
  my $string = join(":",@keys);
  utf8::encode($string);
  return md5_hex($string);
}


=pod

=head2 _swap_keys()

  $newkeyboard = _swap_keys($keyboard,$reloc_list,$n);

Swap one or more pairs ($n randomly sampled pairs) of keys on the keyboard. Lower and upper case characters remain on the same key (e.g. no matter where 'a' is, A is always shift+a). This applies to both letter and non-letter characters (e.g. 1 and ! are always on the same key).

This function returns a new keyboard object with the keys swapped.

=cut

sub _swap_keys {
  my ($keyboard,$reloc_list,$n) = @_;
  my $keyboardcopy = dclone($keyboard);
  $n = 1 if ! $n;
  my $reloc_listsize = @$reloc_list;
  foreach (1..$n) {
    # pick two random keyboard locations from the list of relocatable keys
    my ($key1,$key2);
    while ( $key1 == $key2 || @$key1[2] ne @$key2[2]) {
      $key1 = $reloc_list->[rand($reloc_listsize)];
      $key2 = $reloc_list->[rand($reloc_listsize)];
    }
    # swap these two keys
    _swap_key_pair($keyboardcopy,@$key1,@$key2);
  }
  return $keyboardcopy;
}

=pod

=head2 _swap_key_pair()

  $key1 = [$row1,$col1];
  $key2 = [$row2,$col2];
  _swap_key_pair($keyboard,@$key1,@$key2);

This function modifies $keyboard in place.

=cut

sub _swap_key_pair {
  my ($keyboard,$row1,$col1,$mask1,$row2,$col2,$mask2) = @_;

  my ($k1lc) = @{ $keyboard->{key}[$row1][$col1] }{qw(lc)};
  my ($k2lc) = @{ $keyboard->{key}[$row2][$col2] }{qw(lc)};

  @{$keyboard->{key}[$row1][$col1]}{qw(lc)} = ($k2lc);
  @{$keyboard->{key}[$row2][$col2]}{qw(lc)} = ($k1lc);

  @{$keyboard->{map}}{$k1lc,$k2lc} = @{$keyboard->{map}}{$k2lc,$k1lc};
}

=pod

=head2 calculate_effort()

  my $effort = calculate_effort($triads,$keyboard);

Given a list of triads and the effort matrix, calculate the total carpal effort required to type the document from which the triads were generated. The effort is a non-negative number. The effort is a sum of the efforts for each triad. The total effort is normalized by the number of triads to remove dependency on document size. 

  abcdefg
  abc     -> effort1
   bcd    -> effort2
    cde   -> effort3
      efg -> effort4
  -------    -------
  abcdefg -> total_effort = ( effort1 + effort2 + effort3 + effort4 ) /4

Given a triad xyz, the effort is calculated by the following empirical expression

  effort = e = k1*effort(x) + k2*effort(x)*effort(y) + k3*effort(x)*effort(y)*effort(z) + k4*patheffort(x,y,z)
             = k1*effort(x)*[1 + effort(y)*(k2 + k3*effort(z))] + k4*patheffort(x,y,z)

The form of this expression is motivated by the fact that the effort of three keystrokes is dependent on not only the individual identity of the keys but also alternation of hand, finger, row and column within the triad as well as presence of hard-to-type key combinations (e.g. zxc zqz awz ). For example, it is much easier to type "ttt" than "tbt", since the left forefinger must travel quite a distance in the latter example. Thus the insertion of the "b" character should impact the effort.

In the first-order approximation k2=k3=k4=0 and the effort is simply the effort of typing the first key, effort(x). The individual effort of a key is defined in the <effort_row> blocks and is optionally modified by (a) shift penalty - CAPS are penalized and (b) hand penalty (e.g. you favour typing with your left hand). Since triads overlap, the first-order approximation for the entire document is the sum of the individual key efforts, without any long-range correlations.

The addition of parameters k2 and k3 is designed to raise the effort of repeated difficult-to-type characters. This is where the notion of a triad comes into play. Notice that if effort(x) is zero, then the whole triad effort is zero.

The patheffort(x,y,z) is a penalty which makes less desirable triads in which the keys do not follow a monotonic progression of columns, or triads which do not alternate hands. Once you try to type 'edc' on a qwerty keyboard, or 'erd' you will understand what I mean. The patheffort is a combination of two factors: hand alternation and column alternation. First, define a hand and column flag for a triad

The definition of path effort here is arbitrary. I find that if the hands alternate between each keystroke, typing is easy (e.g. hf=0x). If both hands are used, but don't alternate then it's not as easy, particuarly when some of the columns in the triad are the same (e.g. same finger has to hit two keys like in "jeu"). If the same hand has to be used for three strokes then you're in trouble, particularly when some of the columns repeat. You can redefine the value of the path effort in <path_efforts> block.

=cut 

sub calculate_effort {
  my ($keytriads,$keyboard) = @_;
  die "keyboard not defined " unless $keyboard;
  die "triads not defined" unless $keytriads;
  my $totaleffort = 0;
  my $contributing_triads = 0;
  my @k = @{$CONF{effort_model}{k_param}}{qw(k1 k2 k3 kb kp ks)};
  foreach my $triad (keys %$keytriads) {
    my $triad_effort     = calculate_triad_effort($triad,$keyboard,@k);
    my $num_triads       = $keytriads->{$triad};
    $totaleffort         += $triad_effort * $num_triads ;
    $contributing_triads += $num_triads;
    if($triad =~ /[*]/) {
      #printinfo("calculate_effort",$triad,$num_triads,$triad_effort,$totaleffort,$contributing_triads);
    }
  }
  $totaleffort /= $contributing_triads;
  #printinfo("calculate_effort_done",$totaleffort);
  #printinfo("*"x80);
  return $totaleffort;
}

{

  my $effortlookup = {};

  sub calculate_triad_effort {
    my ($triad,$keyboard,$k1,$k2,$k3,$kb,$kp,$ks) = @_;

    my $leaf = $keyboard->{map};
    # characters of the triad
    my ($c1,$c2,$c3)       = split(//,$triad);
    my ($i1,$i2,$i3)       = ($leaf->{$c1}{idx},$leaf->{$c2}{idx},$leaf->{$c3}{idx});
    if($CONF{memorize} && exists $effortlookup->{$i1}{$i2}{$i3}) {
      return $effortlookup->{$i1}{$i2}{$i3};
    } else {
      #
    }
    # keyboard effort of each character
    my ($be1,$be2,$be3)    = ($leaf->{$c1}{effort}{base},$leaf->{$c2}{effort}{base},$leaf->{$c3}{effort}{base});
    my ($pe1,$pe2,$pe3)    = ($leaf->{$c1}{effort}{penalty},$leaf->{$c2}{effort}{penalty},$leaf->{$c3}{effort}{penalty});
    # finger of each character
    my ($f1,$f2,$f3)       = ($leaf->{$c1}{finger},$leaf->{$c2}{finger},$leaf->{$c3}{finger});
    # row of each character
    my ($row1,$row2,$row3) = ($leaf->{$c1}{row},$leaf->{$c2}{row},$leaf->{$c3}{row});
    # hand of each character 
    my ($h1,$h2,$h3)       = ($leaf->{$c1}{hand},$leaf->{$c2}{hand},$leaf->{$c3}{hand});
    # total triad effort is the sum of base effort product (finger distance) and penalty effort;
    my $triad_effort       = $kb * $k1 * (($be1 + $be2 + $be3)/3 + $k2 * (($be1 * $be2 + $be2 * $be3)/2 + $k3*$be1*$be2*$be3)) + $kp * $k1* (($pe1 + $pe2 + $pe3)/3 + $k2 * (($pe1 * $pe2 + $pe2 * $pe3)/2 + $k3*$pe1*$pe2*$pe3));
    
    if ($ks) {
      # hand, finger, row flags for stroke path
      # see http://mkweb.bcgsc.ca/carpalx/?typing_effort
      my $hand_flag;
      if($h1 == $h3) {
	if($h2 == $h3) {
	  # same hand
	  $hand_flag = 2;
	} else {
	  # alternating
	  $hand_flag = 1;
	}
      } else {
	$hand_flag = 0;
      }

      my $finger_flag;

      if( $f1 > $f2 ) {
	if ( $f2 > $f3 ) {
	  # 1 > 2 > 3 - monotonic all different - pf=0
	  $finger_flag = 0;
	} elsif ( $f2 == $f3 ) {
	  # 1 > 2 = 3 - monotonic some different - pf=1
	  if($c2 eq $c3) {
	    $finger_flag = 1;
	  } else {
	    $finger_flag = 6;
	  }
	} elsif ( $f3 == $f1 ) {
	  $finger_flag = 4;
	} elsif ( $f1 > $f3 && $f3 > $f2 ) {
	  # rolling
	  $finger_flag = 2;
	} else {
	  # not monotonic all different - pf=3
	  $finger_flag = 3;
	}
      } elsif ( $f1 < $f2) {
	if ( $f2 < $f3 ) {
	  # 1 < 2 < 3 - monotonic all different - pf=0
	  $finger_flag = 0;
	} elsif ( $f2 == $f3 ) {
	  if($c2 eq $c3) {
	    # 1 < 2 = 3 - monotonic some different - pf=1
	    $finger_flag = 1;
	  } else {
	    $finger_flag = 6;
	  }
	} elsif ( $f3 == $f1 ) {
	  # 1 = 3 < 2 - not monotonic some different - pf=2
	  $finger_flag = 4;
	} elsif ($f1 < $f3 && $f3 < $f2) {
	  # rolling
	  $finger_flag = 2;
	} else {
	  # not monotonic all different - pf=3
	  $finger_flag = 3;
	}
      } elsif( $f1 == $f2 ) {
	if ( $f2 < $f3 || $f3 < $f1 ) {
	  # 1 = 2 < 3 
	  # 3 < 1 = 2 - monotonic some different - pf=1
	  if($c1 eq $c2) {
	    $finger_flag = 1;
	  } else {
	    $finger_flag = 6;
	  }
	} elsif ( $f2 == $f3 ) {
	  if($c1 ne $c2 && $c2 ne $c3 && $c1 ne $c3) {
	    $finger_flag = 7;
	  } else {
	    # 1 = 2 = 3 - all same - pf=4
	    $finger_flag = 5;
	  }
	}
  }

	my $same_finger_bigrams = 0;

	if(($f1 == $f2 && $c1 ne $c2) || ($f2 == $f3 && $c2 ne $c3)) {
		$same_finger_bigrams++;
	}

    my $row_flag;

      my @dr    = sort { ($b->[0] <=> $a->[0]) || ($a->[1] <=> $b->[1]) } 
	map { [abs($_),$_] } ($row1-$row2,$row1-$row3,$row2-$row3);
      my ($drmax_abs,$drmax) = @{$dr[0]};
      if ($row1 < $row2) {
	if ($row3 == $row2) {
	  # 1 < 2 = 3 - downward with rep
	  $row_flag = 1;
	} elsif ($row2 < $row3) {
	  # 1 < 2 < 3 - downward progression
	  $row_flag = 4;
	} elsif ($drmax_abs == 1) {
	  $row_flag = 3;
	} else {
	  # all/some different - delta row > 1
	  if($drmax < 0) {
	    $row_flag = 7;
	  } else {
	    $row_flag = 5;
	  }
	}
    } elsif ($row1 > $row2) {
      if ($row3 == $row2) {
	# 1 > 2 = 3 - upward with rep
	$row_flag = 2;
      } elsif ($row2 > $row3) {
	# 1 > 2 > 3 - upward
	$row_flag = 6;
      } elsif ($drmax_abs == 1) {
	$row_flag = 3;
      } else {
	if($drmax < 0) {
	  $row_flag = 7;
	} else {
	  $row_flag = 5;
	}
      }
    } else {
      # 1=2
      if($row2 > $row3) {
	# 1 = 2 > 3 - upward with rep
	$row_flag = 2;
      } elsif ($row2 < $row3) {
	# 1 = 2 < 3 - downward with rep
	$row_flag = 1;
      } else {
	# all same
	$row_flag = 0;
      }
    }

    my $path_flag = "$hand_flag$row_flag$finger_flag";
    my $path_cost = $CONF{effort_model}{weight_param}{penalties}{path_offset} + $CONF{effort_model}{path_cost}{$path_flag} + $same_finger_bigrams*$CONF{effort_model}{weight_param}{penalties}{same_finger_bigrams};
    $triad_effort += $ks * $path_cost;

    $CONF{debug} && printdebug(1,"triad $c1$c2$c3 keys $c1 $c2 $c3 base_effort $be1 $be2 $be3 penalty_effort $pe1 $pe2 $pe3 hand $h1 $h2 $h3 row $row1 $row2 $row3 finger $f1 $f2 $f3 ph $hand_flag pr $row_flag pf $finger_flag path $path_flag $path_cost effort $triad_effort");

  } else {

    $CONF{debug} && printdebug(1,"triad $c1$c2$c3 keys $c1 $c2 $c3 base_effort $be1 $be2 $be3 penalty_effort $pe1 $pe2 $pe3 row $row1 $row2 $row3 hand $h1 $h2 $h3 ph - pr - pf - path - 0 effort $triad_effort");

  }
  $effortlookup->{$i1}{$i2}{$i3} = $triad_effort;
  return $triad_effort;

}
}

=pod

=head2 read_document()

 $triads = read_document(); # create hashref to triad frequencies

 $triads->{aab} = frequency of aab;
 $triads->{abc} = frequency of abc;

Read a document from file and create a list of of character triads. Triads are overlapping (more on overlapping below) 3-character combinations. Each triad is stored along with the number of times it appears in the document. All triads are stored, including overlapping triads.

For example, if the document line is

  I am a very lazy dog with big ears.

Then the triads will be

  i am              iam
    am a            ama
     m a v          mav
       a ve         ave
         ver        ver
          ery       ery
           ry l     ryl
            y la    yla
  ...

and so on. Notice that spaces in the document are disregarded during construction of the triads.

Depending on the parse mode, the input document undergoes some transformation before triads are constructed. Each mode must be defined using a <mode_def> block. Three modes are defined and you can add more.

You can control how the triads are read by the <triad> block

  <triad>
  maxnum = 1000 # limit number of triads
  overlap = yes # if set to yes, a triad potentially begins at each character (triads overlap by maximum of 2 characters)
                # if set to no, triads abut 
  </triad>
 
=over

=item mode = perl

If the mode is set to "perl", then all comment lines are disregarded. Comments are identified by lines that begin with #.

=item mode = english

English mode removes all non-alphanumeric characters before constructing triads.

=item mode = letter

All non-letter characters are removed and remaining letters are switched to lower case.

=back

=cut 

sub read_document {

  # prepend script's path before relative paths to the input text
  my $file = shift;
  my $args = shift || {};
  $file = resolve_path($file);

  ################################################################
  # read from JSON, if available

  my $file_triads       = "$file.json";
  if(-e $file_triads) {
        printdebug(1,"reading triads from json",$file_triads);
        my $triads_raw = do{local(@ARGV,$/)=$file_triads;<>};
        return JSON::PP->new->utf8->decode($triads_raw);
  }
	
  ################################################################
  # read from corpus caches, if available
  my $file_cache       = "$file.cache";
  if(-e $file_cache) {
    	printdebug(1,"reading triads from cache",$file_cache);
    	return retrieve($file_cache);
  }

  die "cannot find input document [$file]" unless -e $file;
  die "no permission to read input document [$file]" unless -r $file;
	
  my $keytriads;
  my $triads_count=0;

  open(INPUT,"<:utf8",$file) || die "cannot open input document [$file]";

 READLINE: while(my $line = <INPUT>) {
    chomp $line;
    # remove spaces from text - assume that a space does not influence effort of typing    
    printdebug(2,"read_document","pre-processed", $line);
    $line =~ s/\s//g;
    next unless $line;
    if($CONF{mode_def}{$CONF{mode}}{force_case} eq "lc") {
      $line =~ tr/A-Z/a-z/;
    }
    if($CONF{mode_def}{$CONF{mode}}{reject_char_rx}) {
      (my $rx = $CONF{mode_def}{$CONF{mode}}{reject_char_rx}) =~ s/\/\//\//g;
      $line =~ s/$rx//g;
    }
    if($CONF{mode_def}{$CONF{mode}}{reject_line_rx}) {
      (my $rx = $CONF{mode_def}{$CONF{mode}}{reject_line_rx}) =~ s/\/\//\//g;
      if($line =~ /$rx/) {
				printdebug(2,"read_document","skipping line",$line);
				next READLINE;
      }
    }
    if($CONF{mode_def}{$CONF{mode}}{accept_line_rx}) {
      (my $rx = $CONF{mode_def}{$CONF{mode}}{accept_line_rx}) =~ s/\/\//\//g;
      if($line !~ /$rx/) {
				printdebug(2,"read_document","skipping line",$line);
				next READLINE;
      }
    }
    printdebug(2,"read_document","processed", $line);

		while($line =~ /(...)/g) {
			my $triad = $1;
			if(substr($triad,0,1) eq substr($triad,1,1) && 
				 substr($triad,1,1) eq substr($triad,2,1) && 
				 ! $CONF{mode_def}{$CONF{mode}}{accept_repeats}) {
				# identical triplet - skip if requested 
				printdebug(3,"skipping repeated triad $triad");
			} else {
				$keytriads->{$triad}++;
				$triads_count++;
				printdebug(3,"accepting $triad");
				if($CONF{triads_max_num} && $triads_count >= $CONF{triads_max_num}) {
					$CONF{debug} && printdebug(1,"limiting triads to $CONF{triads_max_num}");
					last READLINE;
				}
			}
			pos $line -= 2 if $CONF{triads_overlap};
		}
  }
  printdebug(1,"found $triads_count triads (",int(keys %$keytriads),"unique)");
  # remove low-frequency triads
  if($CONF{triads_min_freq}) {
    for my $triad (keys %$keytriads) {
      if($keytriads->{$triad} < $CONF{triads_min_freq}) {
				$CONF{debug} && printdebug(1,"removing rare triad",$triad,"freq",$keytriads->{$triad});
				delete $keytriads->{$triad};
      }
    }
  }
  # limit number of triads
  if($CONF{triads_max_num}) {
    $CONF{debug} && printdebug(1,"limiting triads to $CONF{triads_max_num}");
    my @triads_by_freq = sort {$keytriads->{$b} <=> $keytriads->{$a}} keys %$keytriads;
    for my $i ($CONF{triads_max_num} .. @triads_by_freq-1) {
      my $triad_to_del = $triads_by_freq[$i];
      $CONF{debug} && printdebug(1,"removing triad",$triad_to_del,"freq",$keytriads->{$triad_to_del});
      delete $keytriads->{$triad_to_del};
    }
  }
  printdebug(1,"writing triads to cache",$file_cache);
	store($keytriads,$file_cache);

  return $keytriads;
}

################################################################
#
# Dump the keyboard layout to STDOUT
#
################################################################

sub printkeyboard {
  my $keyboard = shift;
  return if $CONF{stdout_quiet};
  print "-"x30,"\n";
  foreach my $row (0..@{$keyboard->{key}}-1) {
    my $lcrow = join(" ",map {$_->{lc}} @{$keyboard->{key}[$row]});
    print $lcrow,"\n";
  }
  print "-"x30,"\n";
}

################################################################
#
# Load effort of hitting each key.
#
# The values here represent the baseline effort. You can add
# a effort offset in create_keyboard(), effectively adding
# a constant to all effort values.
#
# Home row keys require the least effort and therefore have
# the lowest effort. Keys assigned to the pinky have a high
# effort, especially if hitting them also requires wrist
# rotation (e.g. z requires wrist rotation but [ does not).
#
################################################################

sub _parse_finger_distance {
  my $cost;
  my $row = 0;
  my $leaf = $CONF{effort_model}{finger_distance}{row};
  die "typing effort for keyboard rows not defined - you need <effort_row ROW> blocks" unless ref($leaf) eq "HASH";
  for my $row_idx (keys %$leaf) {
    my @keycost = split(" ",$leaf->{$row_idx}{effort});
    map { $cost->[$row_idx-1][ $_ ] = $keycost[$_] } (0..@keycost-1);
  }
  return $cost;
}

=pod

=head2 _parse_keyboard_layout

Parse the <keyboard><row> blocks to determine the location of keys on the keyboard.

Keyboard structure is stored as a hashref. Each key is stored by row/col position

  $keyboard->{key}[ROW][COL]{lc}     = lower case at ROW,COL
  $keyboard->{key}[ROW][COL]{finger} = finger for hitting key at ROW,COL

  $keyboard->{map}{CHAR}{row}    = ROW for key CHAR
  $keyboard->{map}{CHAR}{col}    = COL for key CHAR
  $keyboard->{map}{CHAR}{case}   = CASE of key CHAR
  $keyboard->{map}{CHAR}{finger} = FINGER for hitting key CHAR

=cut

sub _parse_keyboard_layout {
  my $keyboardfile = shift;
  $keyboardfile = resolve_path($keyboardfile);
  die "cannot file keyboard definition - $keyboardfile" unless -e $keyboardfile;
  my %keyboard = _parse_conf_file($keyboardfile);
  die "no keyboard row definitions in keyboard layout" unless $keyboard{keyboard}{row};
  my $keyboard;
  for my $row_idx (sort {$a <=> $b} keys %{$keyboard{keyboard}{row}}) {
      die "no keys defined in keyboard layout for row $row_idx" unless $keyboard{keyboard}{row}{$row_idx}{keys};
      die "no fingers defined in keyboard layout for row $row_idx" unless $keyboard{keyboard}{row}{$row_idx}{fingers};
      my @keys    = split(" ",$keyboard{keyboard}{row}{$row_idx}{keys});
      my @fingers = split(" ",$keyboard{keyboard}{row}{$row_idx}{fingers});
      my $row = $row_idx - 1;
      my $col = 0;
      for my $key_idx (0..@keys-1) {
	  my $key = $keys[$key_idx];
	  my $finger = $fingers[$key_idx];
	  die "undefined finger assignment for keyboard layout for row,col $row_idx,$col" unless defined $finger;
	  my $hand   = $finger > 4 ? 1 : 0;
	  my @char = split(//,$key);
	  printdebug(2,"_parse_keyboard_layout","keyassignment","lc",$char[0],"to row,col",$row,$col);

	  @{$keyboard->{key}[$row][$col]}{qw(row col lc finger hand)} = ($row,$col,@char,$finger,$hand);

	  for my $case (0,1) {
	      @{$keyboard->{map}{$char[$case]}}{qw(row col case finger hand)} = ($row,$col,$case,$finger,$hand);
	  }

	  $col++;
      }
  }
  return $keyboard;
}

################################################################
#
# Parse the keyboard mask, defined in <mask_row N> blocks in the
# configuration file. Each row block should contain an entry
#
# mask = M M M M M ...
#
# where M = 1|0 depending on whether you want the key to be
# eligible (1) or not eligible (0) for remapping. There should
# be as many Ms in each row as there are columns on the keyboard
#
################################################################

sub _parse_mask {
  my $mask;
  for my $row (sort {$a <=> $b} keys %{$CONF{mask_row}}) {
    my $row_idx = $row - 1;
    my @row_mask = split(/[\s]+/,$CONF{mask_row}{$row}{mask});
    for my $col_idx (0..@row_mask-1) {
      $mask->[$row_idx][$col_idx] = $row_mask[$col_idx];
    }
  }
  return $mask;
}

=pod

=head2 make_relocatable_list()

  my $list = make_relocatable_list($mask)

Based on the key mask generated by _parse_mask(), this function returns a list of all keys that can be relocated. The list is a set of row,col pairs.

  $list = [ ... [row,col], [row,col], ... ]

=cut

sub make_relocatable_list {
  my $mask = shift;
  my $list;
  foreach my $row (0..@$mask-1) {
    foreach my $col (0..@{$mask->[$row]}-1) {
      if($mask->[$row][$col]) {
	push @$list, [$row,$col,$mask->[$row][$col]];
      }
    }
  }
  return $list;
}

=pod

=head2 create_keyboard()

  my $keyboard = create_keyboard();

Parses the keyboard layout and creates an array that keeps track of the keys, their positions, character assignments and typing effort. The keyboard array is indexed by row and column of the key and contains a hash

  $keyboard->{key}[row][col]
                           {lc}
                           {row}
                           {col}
                           {effort}

The keyboard layout is read from the <keyboard><row> blocks. The effort in the {key} part of the keyboard object is the canonical effort for the row,col combination as defined in <effort_row> plus any baseline and hand penalties.

The keymap hash is a direct mapping between a character and its position and hand assignment on the keyboard

  $keyboard->{map}{CHAR}
                       {row}
                       {col}
                       {hand}
                       {effort}

The effort in the {map} part of the keyboard object is the effort for the character, based on its row,col combination and includes the shift penalty.

For the standard qwerty layout, look at the keyboard you're using right now (true for >99% of typists). For Dvorak layout, see http://www.mwbrooks.com/dvorak.

=cut

sub create_keyboard {

  my $keyboard_type = shift;
  my $keyboard      = _parse_keyboard_layout($keyboard_type);
  my $cost          = _parse_finger_distance();

  # merge the cost into the keyboard hash 

  my $keyidx=0;
  for my $row_idx (0..@{$keyboard->{key}}-1) {
    for my $col_idx (0..@{$keyboard->{key}[$row_idx]}-1) {

      # this is the canonical cost of typing a key defined in the <effort_row> blocks

      my $base_effort = $cost->[$row_idx][$col_idx];
      my $finger      = $keyboard->{key}[$row_idx][$col_idx]{finger};
      my $hand        = $keyboard->{key}[$row_idx][$col_idx]{hand};
      my $row         = $row_idx;

      $keyboard->{key}[$row_idx][$col_idx]{idx} = $keyidx;

      die "create_keyboard - there is a key defined at row,col $row_idx,$col_idx but no associated effort in the effort file" unless defined $base_effort;
      die "create_keyboard - there is a key defined at row,col $row_idx,$col_idx but no finger assignment" unless defined $finger;
      die "create_keyboard - there is a key defined at row,col $row_idx,$col_idx but no hand assignment" unless defined $hand;

      my $hand_penalty   = $CONF{effort_model}{weight_param}{penalties}{hand}{ $hand ? "right" : "left" };
      my $finger_penalty = $finger < 5 ? 
	(split(/[,\s]+/,$CONF{effort_model}{weight_param}{penalties}{finger}{left}))[$finger] : 
	  (split(/[,\s]+/,$CONF{effort_model}{weight_param}{penalties}{finger}{right}))[$finger-5];
      my $row_penalty    = $CONF{effort_model}{weight_param}{penalties}{row}{$row};

      my ($penalty_effort,$total_effort);

      my $penalty_effort  = 
	$CONF{effort_model}{weight_param}{penalties}{default} + 
	  $CONF{effort_model}{weight_param}{penalties}{weight}{hand}*$hand_penalty +
	    $CONF{effort_model}{weight_param}{penalties}{weight}{row}*$row_penalty +
	      $CONF{effort_model}{weight_param}{penalties}{weight}{finger}*$finger_penalty;

      $total_effort = $CONF{effort_model}{k_param}{kb} * $base_effort + $CONF{effort_model}{k_param}{kp} * $penalty_effort;

      for my $case (0,1) {
	$CONF{debug} && printdebug(1,
				   "create_keyboard","effortassign",
				   "key",$keyboard->{key}[$row_idx][$col_idx]{"lc"},
				   "at row,col",$row_idx,$col_idx,
				   "hand",$hand,
				   "row",$row,
				   "finger",$finger,
				   "base_effort",$base_effort,
				   "base_penalty",$CONF{effort_model}{weight_param}{penalties}{default},
				   "hand_penalty",$CONF{effort_model}{weight_param}{penalties}{weight}{hand},$hand_penalty,
				   "row_penalty",$CONF{effort_model}{weight_param}{penalties}{weight}{row},$row_penalty,
				   "finger_penalty",$CONF{effort_model}{weight_param}{penalties}{weight}{finger},$finger_penalty,
				   "total_penalty",$penalty_effort,
				   "total_effort",$total_effort);
      }
      
      $keyboard->{key}[$row_idx][$col_idx]{effort}{total}   = $total_effort;
      $keyboard->{key}[$row_idx][$col_idx]{effort}{base}    = $base_effort;
      $keyboard->{key}[$row_idx][$col_idx]{effort}{penalty} = $penalty_effort;
      
      $keyboard->{map}{ $keyboard->{key}[$row_idx][$col_idx]{"uc"} }{effort}{total} = $total_effort;
      $keyboard->{map}{ $keyboard->{key}[$row_idx][$col_idx]{"lc"} }{effort}{base} = $base_effort;
      $keyboard->{map}{ $keyboard->{key}[$row_idx][$col_idx]{"lc"} }{effort}{penalty} = $penalty_effort;
      $keyboard->{map}{ $keyboard->{key}[$row_idx][$col_idx]{"lc"} }{idx}    = $keyidx;
      
      $keyidx++;
    }
  }
  return $keyboard;
}

################################################################

################################################################
#
# Housekeeeeeping
#
################################################################

sub populateconfiguration {
  foreach my $key (keys %OPT) {
    $CONF{$key} = $OPT{$key};
  }

  # any configuration fields of the form __XXX__ are parsed and replaced with eval(XXX). The configuration
  # can therefore depend on itself.
  #
  # flag = 10
  # note = __2*$CONF{flag}__ # would become 2*10 = 20

  _parse_conf_data(\%CONF);

}

sub _parse_conf_data {
  my $conf = shift;
  for my $key (keys %$conf) {
    my $value = $conf->{$key};
    if(ref($value) eq "HASH") {
      _parse_conf_data($value);
    } else {
      while($value =~ /__([^_].+?)__/g) {
        my $source = "__" . $1 . "__";
        my $target = eval $1;
        $value =~ s/\Q$source\E/$target/g;
      }
      if($value =~ /^eval\((.*)\)$/) {
        $conf->{$key} = eval $1;
      } else {
        $conf->{$key} = $value;
      }
    }
  }
}

sub _parse_conf_file {
  my $file = shift;
  if(-e $file && -r _) {
    my $conf_obj = new Config::General(-ConfigFile=>$file,
				       -AllowMultiOptions=>1,
				       -LowerCaseNames=>1,
				       -IncludeRelative=>1,
                       -UTF8=>1,
				       -ConfigPath=>[ "$FindBin::RealBin/../etc" ],
				       -AutoTrue=>1);
    return $conf_obj->getall;
  } else {
    die "cannot find or read configuration file $file";
  }
}

sub loadconfiguration {
  my $file = shift;
  my ($scriptname) = fileparse($0);
  $file ||= "$scriptname.conf";
  my @confpath = ("$FindBin::RealBin/../etc/",
		  "$FindBin::RealBin/etc/",
		  "$FindBin::RealBin/",
		  "$ENV{HOME}/.carpalx/etc",
		  "$ENV{HOME}/.carpalx");
  if(-e $file && -r _) {
    # great the file exists
  } else {
    my $found;
    for my $path (@confpath) {
      my $tryfile = "$path/$file";
      if (-e $tryfile && -r _) {
	$file = $tryfile;
	$found=1;
	last;
      }
    }
    return undef unless $found;
  }
  my $conf = new Config::General(-ConfigFile=>$file,
				 -AllowMultiOptions=>"yes",
				 -IncludeRelative=>"yes",
				 -LowerCaseNames=>1,
				 -ConfigPath=>\@confpath,
				 -AutoTrue=>1);
  %CONF = $conf->getall;
  $OPT{configfile} = $file;
  ($CONF{configfile}) = grep($_ =~ /$file$/, $conf->files());
  if($CONF{configfile} !~ /^\//) {
    $CONF{configfile} = sprintf("%s/%s",getcwd,$CONF{configfile});
  }
  $CONF{configdir} = dirname($CONF{configfile});
}

################################################################
#
# $CONF{debug} && printdebug(1,"this is level 1 debug");
# $CONF{debug} && printdebug(2,"this is level 2 debug");
# ...
#
################################################################

sub printdebug {
  my $level = shift;
  if($CONF{debug} >= $level) {
    printinfo("debug",@_);
  }
}

sub printinfo {
  print join(" ",@_),"\n";
}

