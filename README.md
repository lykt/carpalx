# Carpalx keyboard layout optimizer (fork)

Stripped down fork of carpalx keyboard layout optimizer

Original code is available at http://mkweb.bcgsc.ca/carpalx/

Changes:

 * Removed unused code
 * Enabled Unicode
 * Added triad import (from JSON)
 * Enabled composite masks
 * Added optimization examples and triad frequencies for several languages
 * Corrected thumb attribution
 * Added adaptive layouts
