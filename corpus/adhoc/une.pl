use utf8;
binmode STDOUT, ":utf8";
while(my $line = <>) {
	$line =~ s/ /␣/g;
	$line =~ s/(?<=[^␣tscpgwx])m|(?<=[␣tscpgwx])h|^h/ɱ/g;
	$line =~ s/(?<=[eaoiu])n|(?<=[^eaoiu])e|^e/ŋ/g;
	print $line;
}
