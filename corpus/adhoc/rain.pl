use utf8;
binmode STDOUT, ":utf8";
while(my $line = <>) {
	$line =~ s/ /␣/g;
	$line =~ s/(?<=[^t])n|(?<=t)h/ɧ/g;
	$line =~ s/(?<=t)n/h/g;
	$line =~ s/ɧ/n/g;
	print $line;
}
