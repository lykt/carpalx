use utf8;
binmode STDOUT, ":utf8";
while(my $line = <>) {
	$line =~ s/ /␣/g;
	$line =~ s/(?<=[eu])r|(?<=[aoiy])s/h/g;
	print $line;
}
