use utf8;
binmode STDOUT, ":utf8";
while(my $line = <>) {
	$line =~ s/ /␣/g;
	$line =~ s/(?<=[^␣tscpgwx])n|(?<=[␣tscpgwx])h|^h/ɧ/g;
	print $line;
}

