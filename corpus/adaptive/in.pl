use utf8;
binmode STDOUT, ":utf8";
while(my $line = <>) {
	$line =~ s/ /␣/g;
	$line =~ s/(?<=[eaoiu])n/ニ/g;
	$line =~ s/(?<=[^eaoiu])i|^i/ニ/g;
	print $line;
}
