use utf8;
binmode STDOUT, ":utf8";
while(my $line = <>) {
	$line =~ s/ /␣/g;
	$line =~ s/(?<=[eaoiu])n/ナ/g;
	$line =~ s/(?<=[^eaoiu])a|^a/ナ/g;
	print $line;
}
