use utf8;
binmode STDOUT, ":utf8";
while(my $line = <>) {
	$line =~ s/ /␣/g;
	$line =~ s/(?<=[eaoiu])n/ネ/g;
	$line =~ s/(?<=[^eaoiu])e|^e/ネ/g;
	print $line;
}
