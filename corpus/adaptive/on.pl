use utf8;
binmode STDOUT, ":utf8";
while(my $line = <>) {
	$line =~ s/ /␣/g;
	$line =~ s/(?<=[eaoiu])n/ノ/g;
	$line =~ s/(?<=[^eaoiu])o|^o/ノ/g;
	print $line;
}
