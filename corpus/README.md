# Triad frequencies

Triad frequencies are based on processed data dumps from Danish, English, Faroese, Georgian, Icelandic, Norwegian, Swedish and Ukrainian Wikipedia.

The ones for adaptive layouts take into account precedence of adaptive keys.
