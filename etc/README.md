# Optimized layouts


## Classic layouts

Available at [http://mkweb.bcgsc.ca/carpalx/](http://mkweb.bcgsc.ca/carpalx/)

## Carpalx optimizations with preset home keys

Layouts with preset home keys that make it easier to [rearrange](https://lykt.xyz/jnsf/caps/) keys and reuse existing keyboards.

 * [JNSF layout](https://lykt.xyz/jnsf/)
 * [JNRF layout](https://lykt.xyz/jnsf/retro/)
 * [FNRS layout](https://lykt.xyz/jnsf/5/)
 * [JASF layout](https://lykt.xyz/skl/jasf/)
 * [Skarp layouts](https://lykt.xyz/skl/skarp/)
 * [Orthocarpus layout](https://lykt.xyz/jnsf/ortho/)
 * [Eiþ layout](https://lykt.xyz/þorn/#eiþ)

## Adaptive layouts

Layouts with adaptive keys that change state depending on predecessor.

 * [Horn layout](https://lykt.xyz/horn/)
 * [Capricorn layout](https://lykt.xyz/horn/#capricorn)
 * [Orthon layout](https://lykt.xyz/rtsd/#orthon)
 * [Netto layout](https://lykt.xyz/rtsd/#netto)
 * [Ant layout](https://lykt.xyz/rtsd/#ant)
 * [Tin layout](https://lykt.xyz/rtsd/#tin)
 * [Seal layout](https://lykt.xyz/seal/)
 * [Real layout](https://lykt.xyz/seal/#real)
 * [Rina layout](https://lykt.xyz/nari/#rina)
 * [Nari layout](https://lykt.xyz/nari/)
 * [Saint layout](https://lykt.xyz/saint/)

And all Unity and tandem layouts rely adaptive keys.

## Unity layouts

Experimental layouts with carpalx typing effort close to 1

 * [Uno layout](https://lykt.xyz/uno/)
 * [One layout](https://lykt.xyz/uno/#one)
 * [Quant layout](https://lykt.xyz/uno/#quant)
 * [Unit layout](https://lykt.xyz/uno/#unit)

## Tandem layouts

SWORD optimizations with adaptive keys that exchange places depending on predecessor.

 * [Rain layout](https://lykt.xyz/rain/)
 * [Snow layout](https://lykt.xyz/snow/)
 * [Aries layout](https://lykt.xyz/tandem/#aries)
 * [Unicorn layout](https://lykt.xyz/tandem/#unicorn)

## Ortholinear layouts

Layouts optimized for ortholinear keyboards.

 * [Orthostat layout](https://lykt.xyz/orthostat/)
 * [Orthocarpus layout](https://lykt.xyz/jnsf/ortho/)
 * [U-turn layout](https://lykt.xyz/u-turn/)
 * [Norto layouts](https://lykt.xyz/skl/norto/)
 * [Linear layout](https://lykt.xyz/kbd/linear/)
 * [Fullgrid layout](https://lykt.xyz/kbd/aiia/)
 * [Kvant layout](https://lykt.xyz/kvant/)

And all adaptive layouts, Þunder and Tensor layouts are ortholinear optimizations.
 
## Staggered layouts

Layouts optimized for standard keyboards.

 * [Kvikk layouts](https://lykt.xyz/skl/kvikk/)
 * [Handlender layout](https://lykt.xyz/kbd/effort/#bmia)
 * [Median layout](https://lykt.xyz/kbd/effort/#dris)
 * [Equillibrial layout](https://lykt.xyz/kbd/effort/#lrea)

Carpalx optimizations with preset home keys (with exception of Orthocarpus) and Þ layouts are also optimized for standard keyboards.

## Carpalx optimizations with Þ key

 * [Þunder layout](https://lykt.xyz/þorn/#þunder)
 * [Tensor layout](https://lykt.xyz/þorn/#tensor)
 * [Þorn layout](https://lykt.xyz/þorn/)
 * [Goldþorn layout](https://lykt.xyz/þorn/#goldþorn)
 * [Yup layout](https://lykt.xyz/þorn/#yup)
 * [Eiþ layout](https://lykt.xyz/þorn/#eiþ)

