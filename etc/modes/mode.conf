﻿
# modes describe how the input text document is parsed to construct key triads
#
# each mode can have the following set
#
# force_case = no | lc | uc
#
#   forces the case of all characters to lower case (lc) or upper case (uc)
#   force_case = no is an explicit way of stating that the case of the input
#   document characters is not altered
#
# reject_char_rx = REGEX
#   any character that matches the regular expression REGEX is not parsed
#
# Line filters are applied after force_case and reject_char_rx.
#
# reject_line_rx = REGEX
#   any line that matches the regular expression REGEX is not parsed
#
# accept_line_rx = REGEX
#   any line that does not match the regular expression REGEX is not parsed
#
# accept_repeats = yes | no
#   accept or reject repeat triads like aaa, bbb, ccc
#

<mode_def adaptive>
force_case      = lc
reject_char_rx  = [^a-z\.,␣\p{Block: Katakana}]
accept_repeats  = yes
accept_line_rx  = \w
</mode_def>

<mode_def alpha>
force_case      = lc
reject_char_rx  = [^\p{Alpha}␣]
accept_repeats  = yes
accept_line_rx  = \w
</mode_def>

<mode_def common>
force_case      = lc
reject_char_rx  = [^\p{Alpha}\.,␣]
accept_repeats  = yes
accept_line_rx  = \w
</mode_def>

<mode_def en>
force_case      = lc
reject_char_rx  = [^a-z\.,␣]
accept_repeats  = no
accept_line_rx  = \w
</mode_def>

<mode_def fo>
force_case      = lc
reject_char_rx  = [^a-zåøæð´\.,␣]
accept_repeats  = no
accept_line_rx  = \w
</mode_def>

<mode_def is>
force_case      = lc
reject_char_rx  = [^a-zæöþð´\.,␣]
accept_repeats  = no
accept_line_rx  = \w
</mode_def>

<mode_def ka>
reject_char_rx  = [^\p{Block: Georgian}\.,␣]
accept_repeats  = no
</mode_def>

<mode_def no>
force_case      = lc
reject_char_rx  = [^a-zåøæ\.,␣]
accept_repeats  = no
accept_line_rx  = \w
</mode_def>

<mode_def sv>
force_case      = lc
reject_char_rx  = [^a-zåäö\.,␣]
accept_repeats  = no
accept_line_rx  = \w
</mode_def>

<mode_def uk>
force_case      = lc
reject_char_rx  = [^абвгґдеєжзийіїклмнопрстуфхцчшщьюя'\.,␣]
accept_repeats  = no
accept_line_rx  = \w
</mode_def>
